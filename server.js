
//Server configuration

var options = {
    key: fs.readFileSync('backend-server-key.pem'),
    cert: fs.readFileSync('backend-server-cert.pem'),

    // This is necessary only if using the client certificate authentication.
    // Without this some clients don't bother sending certificates at all, some do
    requestCert: true,

    // Do we reject anyone who certs who haven't been signed by our recognised certificate authorities
    rejectUnauthorized: true,

    passphrase : '...abc',

    // This is necessary only if the client uses the self-signed certificate and you care about implicit authorization
    ca: [ fs.readFileSync('../nginx/nginx-cert.pem') ]

};
