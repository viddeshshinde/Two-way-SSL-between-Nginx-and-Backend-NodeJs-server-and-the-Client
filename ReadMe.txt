
*********************************************************************************
Generating Backend Server's private key and self signed certificate for SSL
using openssl:
*********************************************************************************

0. mkdir server

1. cd server

2. openssl genrsa -out backend-server-key.pem -aes256 4096
/*
This will generate private key file (backend-server-key.pem) but before
it will ask for password whose length should be more than 4 and less than
1023 characters. For e.g.: ...abc
*/

3. Enter pass phrase for backend-server-key.pem: ...abc

4. Verifying - Enter pass phrase for backend-server-key.pem: ...abc

5. openssl req -new -key backend-server-key.pem -out backend-server-cert.csr
/*
This will create certificate signing request (backend-server-cert.csr) using
private key generated above. Again the password will be asked of the key
generated above which was '...abc'.
*/

6. Enter pass phrase for backend-server-key.pem: ...abc

Enter the details for generating certificate as below:

-----
7. Country Name (2 letter code) [AU]:IN
8. State or Province Name (full name) [Some-State]:MAHARASHTRA
9. Locality Name (eg, city) []:MUMBAI
10. Organization Name (eg, company) [Internet Widgits Pty Ltd]:ABC PVT. LTD.
11. Organizational Unit Name (eg, section) []:BACKEND SERVER
12. Common Name (e.g. server FQDN or YOUR name) []:www.abc.com
13. Email Address []:

Please enter the following 'extra' attributes
to be sent with your certificate request
14. A challenge password []:
15. An optional company name []:
-----

Note: Don't use IP in Common name. Generate any free domain that will resolve
 to your IP address using any DNS.

16. openssl x509 -req -days 365 -in backend-server-cert.csr -signkey backend-server-key.pem -out backend-server-cert.pem
/*
This will generate self signed certificate (backend-server-cert.pem) which is
signed by server key (backend-server-key.pem).
*/

17. Enter pass phrase for backend-server-key.pem:...abc
/*
Enter the password for the backend server key generated above which is '...abc'.
*/

18. ls
/*
You should have 3 files as:
    1. backend-server-cert.csr
    2. backend-server-cert.pem
    3. backend-server-key.pem
*/



*********************************************************************************
Generating Client's private key and self signed certificate for SSL using
openssl:
*********************************************************************************

0. mkdir client

1. cd client

2. openssl genrsa -out client-key.pem -aes256 4096
/*
This will generate private key file (client-key.pem) but before
it will ask for password whose length should be more than 4 and less than
1023 characters. For e.g.: ...abc
*/

3. Enter pass phrase for client-key.pem: ...abc

4. Verifying - Enter pass phrase for client-key.pem: ...abc

5. openssl req -new -key client-key.pem -out client-cert.csr
/*
This will create certificate signing request (client-cert.csr) using
private key generated above. Again the password will be asked of the key
generated above which was '...abc'.
*/

6. Enter pass phrase for client-key.pem: ...abc

Enter the details for generating certificate as below:

-----
7. Country Name (2 letter code) [AU]:IN
8. State or Province Name (full name) [Some-State]:MAHARASHTRA
9. Locality Name (eg, city) []:MUMBAI
10. Organization Name (eg, company) [Internet Widgits Pty Ltd]:ABC PVT. LTD.
11. Organizational Unit Name (eg, section) []:CLIENT APP
12. Common Name (e.g. server FQDN or YOUR name) []:www.abc2.com
13. Email Address []:

Please enter the following 'extra' attributes
to be sent with your certificate request
14. A challenge password []:
15. An optional company name []:
-----

Note: Don't use IP in Common name. Generate any free domain that will resolve
 to your IP address using any DNS.

16. openssl x509 -req -days 365 -in client-cert.csr -signkey client-key.pem -out client-cert.pem
/*
This will generate self signed certificate (client-cert.pem) which is
signed by client key (client-key.pem).
*/

17. Enter pass phrase for client-key.pem:...abc
/*
Enter the password for the client key generated above which is '...abc'.
*/

18. ls
/*
You should have 3 files as:
    1. client-cert.csr
    2. client-cert.pem
    3. client-key.pem
*/


*********************************************************************************
Generating Nginx's private key and self signed certificate for SSL
using openssl:
*********************************************************************************

0. mkdir nginx

1. cd nginx

2. openssl genrsa -out nginx-key.pem -aes256 4096
/*
This will generate private key file (nginx-key.pem) but before
it will ask for password whose length should be more than 4 and less than
1023 characters. For e.g.: ...abc
*/

3. Enter pass phrase for nginx-key.pem: ...abc

4. Verifying - Enter pass phrase for nginx-key.pem: ...abc

5. openssl req -new -key nginx-key.pem -out nginx-cert.csr
/*
This will create certificate signing request (nginx-cert.csr) using
private key generated above. Again the password will be asked of the key
generated above which was '...abc'.
*/

6. Enter pass phrase for nginx-key.pem: ...abc

Enter the details for generating certificate as below:

-----
7. Country Name (2 letter code) [AU]:IN
8. State or Province Name (full name) [Some-State]:MAHARASHTRA
9. Locality Name (eg, city) []:MUMBAI
10. Organization Name (eg, company) [Internet Widgits Pty Ltd]:ABC PVT. LTD.
11. Organizational Unit Name (eg, section) []:NGINX
12. Common Name (e.g. server FQDN or YOUR name) []:www.abc2.com
13. Email Address []:

Please enter the following 'extra' attributes
to be sent with your certificate request
14. A challenge password []:
15. An optional company name []:
-----

Note: Don't use IP in Common name. Generate any free domain that will resolve
 to your IP address using any DNS.

16. openssl x509 -req -days 365 -in nginx-cert.csr -signkey nginx-key.pem -out nginx-cert.pem
/*
This will generate self signed certificate (nginx-cert.pem) which is
signed by server key (nginx-key.pem).
*/

17. Enter pass phrase for nginx-key.pem:...abc
/*
Enter the password for the nginx key generated above which is '...abc'.
*/

18. ls
/*
You should have 3 files as:
    1. nginx-cert.csr
    2. nginx-cert.pem
    3. nginx-key.pem
*/
